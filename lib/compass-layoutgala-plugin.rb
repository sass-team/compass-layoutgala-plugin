base_directory  = File.expand_path(File.join(File.dirname(__FILE__), '..'))
stylesheets_path = File.join(base_directory, 'stylesheets')

if (defined? Compass)
	Compass::Frameworks.register(
		'blueprint',
		:path => base_directory
	)
else
	ENV["SASS_PATH"] = [ENV["SASS_PATH"], stylesheets_path].compact.join(File::PATH_SEPARATOR)
end
